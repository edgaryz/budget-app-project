# Budget App Project
This app is written in JavaScript ES5.

# Description
App has input fields for income and expenses, 
customer may input values by choosing +/- (income/expense) and check how his balance does, 
how his income and expenses changed and what are expense percentages based on their income.

Simple but handy!